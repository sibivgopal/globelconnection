﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Models
{
    public class GlobalQuestionModel
    {
        public ExamResults model_ExamResults { get; set; }
        public List<GlobalQuestionModel> List_GlobalQuestionModel { get; set; }
        public decimal GlobalQuestionsId { get; set; }
        public Nullable<int> LessionId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Wrong1 { get; set; }
        public string Wrong2 { get; set; }
        public string Wrong3 { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public Nullable<bool> Permission { get; set; }
        public Nullable<bool> Block { get; set; }
        public Nullable<bool> Active { get; set; }

        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public string Option4 { get; set; }
    }
}