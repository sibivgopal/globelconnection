﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Models
{
    public class ExamResults
    {

        public ExamResults Model_ExamResults { get; set; }
        public int Right { get; set; }
        public int Wrong { get; set; }
        public int Skip { get; set; }
        public int TotalMark { get; set; }
        public int TotalQuestion { get; set; }


    }
}