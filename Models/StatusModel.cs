﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Models
{
    public class StatusModel
    {
        public string Success = "success";

        public string Failed = "failed";

        public bool True = true;

        public bool False = false;

    }
}