﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Models
{
    public class UserModel : MesssageModel
    {
        public decimal UserId { get; set; }
        public string SessionId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public Nullable<bool> Avilability { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public Nullable<bool> Permission { get; set; }
        public Nullable<bool> Block { get; set; }
        public Nullable<bool> Active { get; set; }
        public PaytmResponceModel PaytmResponceModel { get; set; }

    }
}