﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Models
{
    public class YouTubeVideoModel
    {
        public decimal VideoId { get; set; }
        public Nullable<decimal> LessionId { get; set; }
        public string VideoName { get; set; }
        public string VideoURL { get; set; }
        public Nullable<bool> Avilability { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public Nullable<bool> Permission { get; set; }
        public Nullable<bool> Block { get; set; }
        public Nullable<bool> Active { get; set; }
        public string VideoId_String { get; set; }

        public List<YouTubeVideoModel> List_YouTubeVideoModel { get; set; }
    }
}