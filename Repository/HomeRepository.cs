﻿using Globalknowledge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Repository
{
    public class HomeRepository : BaseRepository
    {
        public List<YouTubeVideoModel> GlobalKnowledge_Videos_GetAll()
        {
            try
            {
                List<YouTubeVideoModel> li = new List<YouTubeVideoModel>();
                var datas = _Entities.YouTubeVideos.Where(x => x.Block == false && x.Active == true).ToList();
                foreach (var a1 in datas)
                {
                    YouTubeVideoModel mo = new YouTubeVideoModel
                    {
                        VideoId = a1.VideoId,
                        VideoId_String = SessionRepository.Encrypt(a1.VideoId.ToString()),
                        LessionId = a1.LessionId,
                        VideoName = a1.VideoName,
                        VideoURL = a1.VideoURL,
                    };                    
                    li.Add(mo);
                }
                return li;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<GlobalQuestionModel> GlobalKnowledge_Exams(string values, int indiger)
        {
            try
            {
                Random optio = new Random();
                Random Quest = new Random();

                values = SessionRepository.Decrypt(values);
                long viedioId = Convert.ToInt64(values); 
                List<GlobalQuestionModel> li = new List<GlobalQuestionModel>();
                var datas = _Entities.GlobalQuestions.Where(x => x.VideoId == viedioId && x.Block == false && x.Active == true).ToList();

                var ranDatas = datas.OrderBy(x => Quest.Next()).Take(indiger).ToList();
                int count = 1;
                foreach (var a1 in ranDatas)
                {
                    string [] ranArr = new string[4];
                    ranArr[0] = a1.Answer;
                    ranArr[1] = a1.Wrong1;
                    ranArr[2] = a1.Wrong2;
                    ranArr[3] = a1.Wrong3;

                    
                    string[] MyRandomArray = ranArr.OrderBy(x => optio.Next()).ToArray();

                    GlobalQuestionModel mo = new GlobalQuestionModel
                    {
                        GlobalQuestionsId =   a1.GlobalQuestionsId,                        
                        Question = count.ToString() + "." + a1.Question,
                        Option1 = MyRandomArray[0],
                        Option2 = MyRandomArray[1],
                        Option3 = MyRandomArray[2],
                        Option4 = MyRandomArray[3],
                    };                    
                    li.Add(mo);

                    count = count + 1;
                }
                return li;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ExamResults GlobalKnowledge_Exams_Results(GlobalQuestionModel model, UserModel datas)
        {
            try
            {
                int right = 0;
                int wrong = 0;
                ExamResults mo = new ExamResults();
                mo = model.model_ExamResults;
               // mo.TotalQuestion = model.List_GlobalQuestionModel.Count();
                foreach (var a1 in model.List_GlobalQuestionModel)
                {
                    var results = _Entities.GlobalQuestions.Where(x => x.GlobalQuestionsId == a1.GlobalQuestionsId
                                   && x.Answer == a1.Answer && x.Block == false && x.Active == true).FirstOrDefault();
                    if (results != null)
                    {
                        right +=  1;
                    }
                    else if (results == null)
                    {
                        wrong += 1;
                    }
                }

                mo.Right = right;
                mo.Wrong = wrong;
                mo.Skip =  mo.TotalQuestion - (right + wrong);
                mo.TotalQuestion = mo.TotalQuestion;

                var UserHistory = _Entities.UserMarkHistories.Create();
                UserHistory.UserId = datas.UserId;
                UserHistory.Name = datas.Name;
                UserHistory.Address = datas.Address;
                UserHistory.Email = datas.Email;
                //UserHistory.Lession = "Null";
                UserHistory.RightAnswer = mo.Right.ToString();
                UserHistory.WrongAnswer = mo.Wrong.ToString();
                UserHistory.SkipAnswer = mo.Skip.ToString();
                UserHistory.TotalQuestion = mo.TotalQuestion.ToString();
                UserHistory.Avilability = true;
                UserHistory.CreateDate = DateTime.Now.ToString();
                UserHistory.Permission = true;
                UserHistory.Block = false;
                UserHistory.Active = true;
                _Entities.UserMarkHistories.Add(UserHistory);
                _Entities.SaveChanges();



                //mo.                
                return mo;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<UserMarkHistoryModel> Dashboard(UserModel model)
        {
            try
            {
                List<UserMarkHistoryModel> li = new List<UserMarkHistoryModel>();
                var results = _Entities.UserMarkHistories.Where(x => x.UserId == model.UserId && x.Block == false && x.Active == true).OrderByDescending(x => x.UserMarkHistoryId).ToList();
                if (results.Count() > 0)
                {
                    foreach (var a1 in results)
                    {
                        DateTime dt = Convert.ToDateTime(a1.CreateDate);
                        string dd = dt.Day.ToString();
                        string mm = dt.Month.ToString();
                        string yy = dt.Year.ToString();
                        string markhistoryId = SessionRepository.Encrypt(a1.UserMarkHistoryId.ToString());
                        UserMarkHistoryModel mo = new UserMarkHistoryModel
                        {

                            MarkHistoryId = markhistoryId,
                            CreateDate = dd + "-" + mm + "-" + yy,
                            RightAnswer = a1.RightAnswer,
                            WrongAnswer = a1.WrongAnswer,
                            SkipAnswer = a1.SkipAnswer,
                            TotalQuestion = a1.TotalQuestion
                        };

                        li.Add(mo);
                    }
                }
                

                return li;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public UserMarkHistoryModel model_UserMarkHistoryModel(UserModel model)
        {
            try
            {
                var results = _Entities.UserMarkHistories.Where(x => x.UserId == model.UserId && x.Block == false && x.Active == true).OrderByDescending(x => x.UserMarkHistoryId).ToList();
                if (results.Count() > 0)
                {
                    int rightAnswer = 0;
                    int SkipAnswer = 0;
                    int wrongAnswer = 0;
                    int totalQuestions = 0;
                    foreach (var a1 in results)
                    {
                        rightAnswer +=  Convert.ToInt32(a1.RightAnswer);
                        SkipAnswer +=  Convert.ToInt32(a1.SkipAnswer);
                        wrongAnswer +=  Convert.ToInt32(a1.WrongAnswer);
                        totalQuestions += Convert.ToInt32(a1.TotalQuestion);
                    }
                    UserMarkHistoryModel mo = new UserMarkHistoryModel
                    {
                        RightAnswer = (((rightAnswer * 100) / totalQuestions).ToString())+"%",
                        SkipAnswer = (((SkipAnswer * 100)/totalQuestions).ToString())+"%",
                        WrongAnswer = (((wrongAnswer * 100) / totalQuestions).ToString())+"%",
                        TotalQuestion = totalQuestions.ToString()
                    };

                    return mo;
                }
                else
                {
                    UserMarkHistoryModel mo = new UserMarkHistoryModel
                    {
                        RightAnswer = "0%",
                        SkipAnswer = "0%",
                        WrongAnswer = "0%",
                        TotalQuestion = "0"
                    };

                    return mo;
                }

            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public MesssageModel DashBordDeleteHistory(UserMarkHistoryModel model)
        {
            try
            {
                long historyId = Convert.ToInt64(SessionRepository.Decrypt(model.MarkHistoryId));
                var results = _Entities.UserMarkHistories.Where(x => x.UserMarkHistoryId == historyId).FirstOrDefault();
                if (results != null)
                {
                    results.Permission = false;
                    results.Block = true;
                    results.UpdateDate = DateTime.Now.ToString();
                    results.Active = false;
                    _Entities.SaveChanges();

                    MesssageModel.message = "success";
                    MesssageModel.status = true;
                    return MesssageModel;
                }
                else
                {
                    MesssageModel.message = "failed";
                    MesssageModel.status = false;
                    return MesssageModel;
                }
                                
            }
            catch(Exception ex)
            {
                MesssageModel.message = ex.Message;
                MesssageModel.status = false;
                return MesssageModel;
            }
        }

    }
}