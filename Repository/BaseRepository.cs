﻿using Globalknowledge.Database;
using Globalknowledge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Repository
{
    public class BaseRepository
    {
       public GlobalknowledgeEntities _Entities = new GlobalknowledgeEntities();
       public StatusModel StatusModel = new StatusModel();
       public MesssageModel MesssageModel = new MesssageModel();
        public List<UserModel> User_SelectAll()
        {
            List<UserModel> lis = new List<UserModel>();
            try
            {
                
                var datas =  _Entities.Users.Where(x => x.Block == false && x.Active == true).ToList();
                if (datas.Count > 0)
                {
                    foreach (var a1 in datas)
                    {
                        UserModel mo = new UserModel();
                        mo.UserId = a1.UserId;
                        mo.SessionId = a1.SessionId;
                        mo.Name = a1.Name;
                        mo.Address = a1.Address;
                        mo.Email = a1.Email;
                        mo.Password = a1.Password;
                        mo.Phone = a1.Phone;
                        mo.Avilability = a1.Avilability;
                        mo.CreateDate = a1.CreateDate;
                        mo.UpdateDate = a1.UpdateDate;
                        mo.Permission = a1.Permission;
                        mo.Block = a1.Block;
                        mo.Active = a1.Active;

                        mo.message = StatusModel.Success;
                        mo.status = StatusModel.True;

                        lis.Add(mo);

                    }
                }
                else
                {
                    UserModel mo = new UserModel();
                    mo.status = StatusModel.False;
                    mo.message = StatusModel.Failed;
                    lis.Add(mo);
                    return lis;
                }
                
                return lis;
            }
            catch (Exception ex)
            {
                UserModel mo = new UserModel();
                mo.status = StatusModel.False;
                mo.message = ex.Message;
                lis.Add(mo);
                return lis;
            }
        }

        public UserModel User_Email_Password_Single(UserModel model)
        {
            UserModel mo = new UserModel();
            try
            {
                var datas = _Entities.Users.Where(x => x.Email ==model.Email && x.Password == model.Password &&  x.Block == false && x.Active == true).FirstOrDefault();

                if (datas != null)
                {
                    
                    mo.UserId = datas.UserId;
                    mo.SessionId = datas.SessionId;
                    mo.Name = datas.Name;
                    mo.Address = datas.Address;
                    mo.Email = datas.Email;
                    mo.Password = datas.Password;
                    mo.Phone = datas.Phone;
                    mo.Avilability = datas.Avilability;
                    mo.CreateDate = datas.CreateDate;
                    mo.UpdateDate = datas.UpdateDate;
                    mo.Permission = datas.Permission;
                    mo.Block = datas.Block;
                    mo.Active = datas.Active;
                    
                    

                    mo.message = StatusModel.Success;
                    mo.status = StatusModel.True;
                }
                else
                {                     
                    mo.message = StatusModel.Failed;
                    mo.status = StatusModel.False;
                }                
               
            }
            catch (Exception ex)
            {
                mo.message = ex.Message;
                mo.status = StatusModel.False;
            }

            return mo;
        }

        public UserModel User_ChekSession(UserModel model)
        {
            UserModel mo = new UserModel();
            try
            {
                var datas = _Entities.Users.Where(x => x.UserId == model.UserId && x.SessionId == model.SessionId && x.Block == false && x.Active == true).FirstOrDefault();

                if (datas != null)
                {                                        
                    mo.message = StatusModel.Success;
                    mo.status = StatusModel.True;
                }
                else
                {
                    mo.message = StatusModel.Failed;
                    mo.status = StatusModel.False;
                }

            }
            catch (Exception ex)
            {
                mo.message = ex.Message;
                mo.status = StatusModel.False;
            }

            return mo;
        }


        
    }
}