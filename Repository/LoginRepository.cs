﻿using Globalknowledge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globalknowledge.Repository
{
    public class LoginRepository : BaseRepository
    {
        public UserModel UpdateSessionId(UserModel model)
        {
            UserModel mo = new UserModel();
            try
            {
                var datas = _Entities.Users.Where(x => x.UserId == model.UserId  && x.Block == false && x.Active == true).FirstOrDefault();

                if (datas != null)
                {
                    string sessionId = SessionRepository.SessionId();
                    datas.SessionId = sessionId;
                    _Entities.SaveChanges();
                    mo.SessionId = sessionId;
                    mo.message = StatusModel.Success;
                    mo.status = StatusModel.True;
                }
                else
                {
                    mo.message = StatusModel.Failed;
                    mo.status = StatusModel.False;
                }

            }
            catch (Exception ex)
            {
                mo.message = ex.Message;
                mo.status = StatusModel.False;
            }

            return mo;
        }
    }
}