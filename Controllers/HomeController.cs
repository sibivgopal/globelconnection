﻿using Globalknowledge.Models;
using Globalknowledge.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Globalknowledge.Controllers
{
    public class HomeController : Controller
    {
        //Flow..................\
        /*  var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }
                
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }*/
        //....................
        HomeRepository HomeRepository = new HomeRepository();
        public ActionResult Home()
        {            
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }
                
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            
        }

        public ActionResult Exams()
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
            
        }
        public ActionResult Videos()
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }

        }
        public ActionResult Dashboard()
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    UserMarkHistoryModel mo = new UserMarkHistoryModel {

                        list_UserMarkHistoryModel = HomeRepository.Dashboard(datas),
                        model_UserMarkHistoryModel = HomeRepository.model_UserMarkHistoryModel(datas)
                    };

                    return View(mo);
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult GlobalKnowledge_Videos()
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    YouTubeVideoModel mo = new YouTubeVideoModel();
                    mo.List_YouTubeVideoModel = HomeRepository.GlobalKnowledge_Videos_GetAll();
                    return View(mo);
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult GlobalKnowledge_Exams(string values)
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {

                    Session["GlobalKnowledgeExamsValue"] = values;
                                
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

        [HttpPost]
        public JsonResult GlobalKnowledge_Exams_Questions(CommenModel model)
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    int intiger = Convert.ToInt32(model.StringData1);
                    string values = Session["GlobalKnowledgeExamsValue"].ToString();
                    GlobalQuestionModel mo = new GlobalQuestionModel
                    {

                        List_GlobalQuestionModel = HomeRepository.GlobalKnowledge_Exams(values, intiger)
                    };
                    return Json(mo, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionExpired", JsonRequestBehavior.AllowGet);
                   // return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return Json("Login", JsonRequestBehavior.AllowGet);
               /// return RedirectToAction("Login", "Login");
            }
        }

        [HttpPost]
        public JsonResult GlobalKnowledge_Exams_Results(GlobalQuestionModel model)
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    ExamResults mo = new ExamResults /// this is starting......
                    {
                        Model_ExamResults = HomeRepository.GlobalKnowledge_Exams_Results(model, datas)
                    };

                    return Json(mo, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionExpired", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("Login", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DashBordDeleteHistory(UserMarkHistoryModel model)
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    var results = HomeRepository.DashBordDeleteHistory(model);
                   
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("SessionExpired", JsonRequestBehavior.AllowGet);
                    // return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return Json("Login", JsonRequestBehavior.AllowGet);
                // return RedirectToAction("Login", "Login");
            }
        }

        [HttpPost]
        public ActionResult Payment()
        {
            var datas = (UserModel)Session["UserLoginDemo"];
            if (datas != null)
            {
                var checkSession = HomeRepository.User_ChekSession(datas);
                if (checkSession.status == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("SessionExpired", "Login");
                }

            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

    }
}