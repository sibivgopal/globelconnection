﻿using Globalknowledge.Models;
using Globalknowledge.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Globalknowledge.Controllers
{
    public class LoginController : Controller
    {
        LoginRepository LoginRepository = new LoginRepository();
        // GET: Login Testin
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult SignIn(UserModel model)
        {
            try
            {                 
                var results = LoginRepository.User_Email_Password_Single(model);
                if (results.status == true)
                {    
                   

                    var UpdateSessionId = LoginRepository.UpdateSessionId(results);
                    if (UpdateSessionId.status == true)
                    {
                        results.SessionId = UpdateSessionId.SessionId;
                        
                        Session["UserLoginDemo"] = results;
                        return RedirectToAction("Home", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Login", "Login");
                    }
                                       
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }

            }
            catch (Exception Ex)
            {
                return RedirectToAction("Login", "Login");

            }

        }

        public ActionResult SignUp()
        {
            return View();
        }
        public ActionResult SessionExpired()
        {
            return View();
        }

        public ActionResult Logout()
        {
            try
            {
                var datas = (UserModel)Session["UserLoginDemo"];
                if (datas != null)
                {
                    var UpdateSessionId = LoginRepository.UpdateSessionId(datas);
                    if (UpdateSessionId.status == true)
                    {
                        Session.Abandon();
                        Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        Session.Abandon();
                        Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }

                }
                else
                {
                    Session.Abandon();
                    Session.Clear();
                    return RedirectToAction("Login", "Login");
                }

                
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Login");
            }
             
        }

    }
}