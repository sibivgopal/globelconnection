﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Globalknowledge.Controllers
{
    public class TempLayoutController : Controller
    {
        // GET: TempLayout
        public ActionResult DashBoard()
        {
            return View();
        }
        public ActionResult UIElements()
        {
            return View();
        }
        public ActionResult TablePanel()
        {
            return View();
        }
        public ActionResult MorisChart()
        {
            return View();
        }
        public ActionResult TableExsample()
        {
            return View();
        }
        public ActionResult Forms()
        {
            return View();
        }
        public ActionResult BlankPage()
        {
            return View();
        }
    }
}